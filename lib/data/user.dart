import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  final String displayName;
  final String email;
  final String lastSeen;
  final String photoURL;
  final String uid;
  final String username;

  User({
    this.displayName,
    this.email,
    this.lastSeen,
    this.photoURL,
    this.uid,
    this.username,
  });

  factory User.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;

    return User(
      uid: doc.documentID,
      displayName: data['displayName'] ?? 'User',
      email: data['email'] ?? '',
      lastSeen: data['lastSeen'] ?? '',
      photoURL: data['photoURL'] ?? '',
      username: data['username'] ?? 'user',
    );
  }

  factory User.fromMap(Map data) {
    return User(
      uid: data['uid'] ?? '',
      displayName: data['displayName'] ?? 'User',
      email: data['email'] ?? '',
      lastSeen: data['lastSeen'] ?? '',
      photoURL: data['photoURL'] ?? '',
      username: data['username'] ?? 'user',
    );
  }
}
