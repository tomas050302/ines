import 'package:flutter/material.dart';
import 'package:myyda/Messages.dart';
import 'package:myyda/screens/login.dart';
import 'package:myyda/screens/register.dart';
import 'package:simple_animations/simple_animations.dart';
import '../utils/Auth.dart';

class Landing extends StatefulWidget {
  Landing({Key key}) : super(key: key);

  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> with AnimationControllerMixin {
  Map<String, dynamic> _profile;

  final Auth _auth = Auth();

  @override
  void initState() {
    super.initState();

    // Subscriptions are created here
    _auth.profile.listen((state) => setState(() => _profile = state));

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage("assets/images/landing.jpg"),
        fit: BoxFit.cover,
      )),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Image.asset(
            "assets/images/logo-white.png",
            width: 150,
            alignment: Alignment.center,
          ),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Column(
              children: <Widget>[
                ControlledAnimation(
                    duration: Duration(seconds: 2),
                    tween: Tween(begin: 0.0, end: 1.0),
                    builder: (context, value) {
                      return Opacity(
                        opacity: value,
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Make Your\nStore\nShine",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 50,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      );
                    }),
                SizedBox(
                  height: 50,
                ),
                Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                            color: Colors.white,
                          )),
                      width: 150,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Login(),
                            ),
                          );
                        },
                        child: Text(
                          "LOGIN",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: Messages.buttonFontSize,
                          ),
                        ),
                        elevation: 0,
                        highlightColor: Colors.white,
                        highlightElevation: 0.0,
                        color: Colors.transparent,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                            color: Colors.white,
                          )),
                      width: 150,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Register(),
                            ),
                          );
                        },
                        child: Text(
                          "REGISTER",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: Messages.buttonFontSize,
                          ),
                        ),
                        elevation: 0,
                        highlightColor: Colors.white,
                        highlightElevation: 0.0,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
