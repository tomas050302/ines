import 'package:flutter/material.dart';
import 'package:myyda/screens/createShop.dart';
import 'package:myyda/ui/topbar.dart';

class newShop extends StatefulWidget {
  newShop({Key key}) : super(key: key);

  _newShopState createState() => _newShopState();
}

class _newShopState extends State<newShop> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Topbar(),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Image.asset(
              'assets/images/shopping-basket.png',
              width: 150.0,
              alignment: Alignment.center,
            ),
          ),
          SizedBox(
            height: 2.0,
          ),
          Text(
            "Ups, you don't have any shop",
            style: TextStyle(
              fontSize: 23.0,
              fontWeight: FontWeight.bold,
              color: Colors.redAccent,
            ),
          ),
          SizedBox(
            height: 50.0,
          ),
          RaisedButton(
            child: Text(
              "Create a new shop",
              style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                color: Colors.lightGreen,
              ),
            ),
            onPressed: () => Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (BuildContext context) => createShop())),
            elevation: 0,
            highlightElevation: 0,
            color: Colors.transparent,
          )
        ],
      ),
    );
  }
}
