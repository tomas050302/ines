import 'package:myyda/Messages.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Validations {

  static final _db = Firestore.instance;

   // Validates if the password is empty
  static String validatePasswordLogin(String value) {
    return value.isEmpty ? Messages.formErrorPasswordEmpty : null;
  }
  
  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return Messages.formErrorInvalidEmail;
    else
      return null;
  }

  static String validateUsername(String username) {

    Pattern pattern = r'^(?!.*__.*)(?!.*\.\..*)[a-z0-9_.]+$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(username))
      return Messages.formErrorInvalidUsername;
    else
      _db.collection('users').snapshots().listen((data) => {
        data.documents.forEach((doc) {
          // ignore: sdk_version_set_literal
          if(doc.data['username'].toString().toLowerCase() == username.toLowerCase()) {
              return Messages.formErrorUsernameAlreadyExistss;
          }
        })
      });

    return null;

  }


}